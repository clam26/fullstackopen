import React from 'react';
import ReactDOM from 'react-dom';

const Hello = (props) => {
    const {name, age} = props
    return (
        <>
            <div>Hello {name} dalla componente Hello</div>
            <div>you are {age} years old</div>
        </>
    )
}

const App = () => {
    const a = 10
    const b = 20
    const now = new Date()
    return (
        <div>
            <p>Greetings</p>
            <Hello name = "Rasy" age = "30"/>
            <Hello name = "Resy" age = "31"/>
            <Hello name = "Risy" age = "32"/>
            <Hello name = "Rosy" age = "33"/>
            <Hello name = "Rusy" age = "34"/>

            <p>Oggi è {now.toString()}</p>
            <p> Il risultato di {a} + {b} è {a+b}</p>
        </div>
    )
}
ReactDOM.render(<App />, document.getElementById('root'));